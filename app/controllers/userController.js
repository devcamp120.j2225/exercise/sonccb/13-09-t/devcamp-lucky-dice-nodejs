// Import thư viện mongoose
const mongoose = require("mongoose");

// Import User Model
const userModel = require("../models/userModel");

// Create User
const createUser = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let body = req.body;

 // B2: Validate dữ liệu
 if(!body.username) {
     return res.status(400).json({
         message: "User name is required!"
     })
 }
 if(!isNaN(body.username)) {
  return res.status(400).json({
     message: "User name is String!"
  })
 }
 if(!body.firstname) {
  return res.status(400).json({
      message: "First name is required!"
  })
}
if(!isNaN(body.firstname)) {
 return res.status(400).json({
    message: "First Name is String!"
 })
}
if(!body.lastname) {
 return res.status(400).json({
     message: "Last name is required!"
 })
}
if(!isNaN(body.lastname)) {
 return res.status(400).json({
    message: "Last name is String!"
 })
}

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let newUserData = {
     _id: mongoose.Types.ObjectId(),
     username: body.username,
     firstname: body.firstname,
     lastname: body.lastname
 }

 userModel.create(newUserData, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Create successfully",
         newUser: data
     })
 })
}

// Get all User 
const getAllUser = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 // B2: Validate dữ liệu
 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 userModel.find((error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Get all User successfully",
         Users: data
     })
 })
}

// Get User by id
const getUserById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let userId = req.params.userId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(userId)) {
     return res.status(400).json({
         message: "User ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 userModel.findById(userId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: `Get Users ID ${userId} successfully`,
         User: data
     })
 })
}

// Update User by id
const updateUserById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let userId = req.params.userId;
 let body = req.body;

 // B2: Validate dữ liệu

if(!isNaN(body.username)) {
    return res.status(400).json({
       message: "User name is String!"
    })
 }
if(!isNaN(body.firstname)) {
    return res.status(400).json({
       message: "First Name is String!"
    })
 }
if(!isNaN(body.lastname)) {
    return res.status(400).json({
       message: "Last name is String!"
    })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let userUpdate = {
   //_id: mongoose.Types.ObjectId(),
   username: body.username,
   firstname: body.firstname,
   lastname: body.lastname
 };

 userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Update User successfully",
         updatedUser: data
     })
 })
}

// Delete User by id
const deleteUserById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let userId = req.params.userId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(userId)) {
     return res.status(400).json({
         message: "User ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 userModel.findByIdAndDelete(userId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(204).json({
         message: "Delete User successfully"
     })
 })
}

// Export User controller thành 1 module
module.exports = {
   createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}
