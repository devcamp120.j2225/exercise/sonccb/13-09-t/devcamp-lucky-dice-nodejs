// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Prize Model
const prizeModel = require("../models/prizeModel");

// Create Prize
const createPrize = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let body = req.body;

 // B2: Validate dữ liệu
 if(!body.name) {
     return res.status(400).json({
         message: "Name is required!"
     })
 }
 if(!isNaN(body.name)) {
  return res.status(400).json({
     message: "Name must be String!"
  })
 }
 

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let newPrizeData = {
     _id: mongoose.Types.ObjectId(),
     name: body.name,
     description: body.description
 }

 prizeModel.create(newPrizeData, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Create successfully",
         newPrize: data
     })
 })
}

// Get all Prize 
const getAllPrize = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 // B2: Validate dữ liệu
 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 prizeModel.find((error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Get all Prize successfully",
         Prizes: data
     })
 })
}

// Get Prize by id
const getPrizeById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let prizeId = req.params.prizeId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(prizeId)) {
     return res.status(400).json({
         message: "Prize ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 prizeModel.findById(prizeId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: `Get Prizes ID ${prizeId} successfully`,
         Prize: data
     })
 })
}

// Update Prize by id
const updatePrizeById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let prizeId = req.params.prizeId;
 let body = req.body;

 // B2: Validate dữ liệu
 if (!mongoose.Types.ObjectId.isValid(prizeId) ) {
   return res.status(400).json({
       status: "Error 400: Bad Request",
        message:"Prize ID is invalid"
   })
 }
 if (!isNaN(body.name)) {
   return res.status(400).json({
       status: "Error 400: Bad Request",
        message:"Name must be String"
   })
 }
 if (!isNaN(body.note)) {
   return res.status(400).json({
       status: "Error 400: Bad Request",
        message:"Note must be String"
   })
 }
 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let PrizeUpdate = {
   //_id: mongoose.Types.ObjectId(),
   name: body.name,
<<<<<<< HEAD
   firstname: body.firstname,
   lastname: body.lastname
=======
   note: body.note
>>>>>>> c1a1f5b43d03c6a77a986874152ff20a789083d1
 };

 prizeModel.findByIdAndUpdate(prizeId, PrizeUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Update Prize successfully",
         updatedPrize: data
     })
 })
}

// Delete Prize by id
const deletePrizeById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let prizeId = req.params.prizeId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(prizeId)) {
     return res.status(400).json({
         message: "Prize ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 prizeModel.findByIdAndDelete(prizeId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(204).json({
         message: "Delete Prize successfully"
     })
 })
}

// Export Prize controller thành 1 module
module.exports = {
   createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}
