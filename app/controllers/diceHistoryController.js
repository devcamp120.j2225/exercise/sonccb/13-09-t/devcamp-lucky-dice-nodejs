// Import thư viện mongoose
const mongoose = require("mongoose");

// Import User Model
const diceHistoryModel = require("../models/diceHistoryModel");
const userModel = require("../models/userModel");

const createDiceHistory = (request, response) => {
 //B1: Chuẩn bị dữ liệu
 let body = request.body;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(body.userId)) {
     return response.status(400).json({
         status: "Error 400: Bad Request",
         message: "User ID is invalid"
     })
 }

 if (!(Number.isInteger(body.dice) && body.dice  && body.dice > 0 && body.dice <= 6 )) {
    return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "Dice is invalid"
    })
 }

 //B3: Thao tác với cơ sở dữ liệu
 let newDiceHistory = {
     _id: mongoose.Types.ObjectId(),
     dice: body.dice,
     user: body.userId
 }

 diceHistoryModel.create(newDiceHistory, (error, data) => {
     if(error) {
         return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return response.status(201).json({
            status: "Get data success",
            data: data
         })
     }
 })
}

const getAllDiceHistory = (request, response) => {
 //B1: Chuẩn bị dữ liệu
 //B2: Validate dữ liệu
 //B3: Thao tác với cơ sở dữ liệu
 diceHistoryModel.find((error, data) => {
    if(error) {
        return response.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
        })
    } else {
        return response.status(200).json({
            status: "Success: Get dice history success",
            data: data
        })
    }
})
}

const getDiceHistoryById = (request, response) => {
 //B1: Chuẩn bị dữ liệu
 let diceHistoryId = request.params.diceHistoryId;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
     return response.status(400).json({
         status: "Error 400: Bad Request",
         message: "Dice History ID is not valid"
     })
 }
 //B3: Thao tác với cơ sở dữ liệu
 diceHistoryModel.findById(diceHistoryId, (error, data) => {
     if(error) {
         return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return response.status(200).json({
             status: "Success: Get dice history success",
             data: data
         })
     }
 })
}

const updateDiceHistoryById = (request, response) => {
 //B1: Chuẩn bị dữ liệu
 let diceHistoryId = request.params.diceHistoryId;
 let bodyRequest = request.body;

 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
     return response.status(400).json({
         status: "Error 400: Bad Request",
         message: "Dice History ID is not valid"
     })
 }
 if (!(Number.isInteger(body.dice) && body.dice  && body.dice > 0 && body.dice <= 6 )) {
    return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "Dice is invalid"
    })
 }
 //B3: Thao tác với cơ sở dữ liệu
 let diceHistoryUpdate = {
     dice: bodyRequest.dice
 }

 diceHistoryModel.findByIdAndUpdate(diceHistoryId, diceHistoryUpdate, (error, data) => {
     if(error) {
         return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         return response.status(200).json({
             status: "Success: Update Dice History success",
             data: data
         })
     }
 })
}

const deleteDiceHistoryById = (request, response) => {
 //B1: Chuẩn bị dữ liệu
 let diceHistoryId = request.params.diceHistoryId;
 //B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
     return response.status(400).json({
         status: "Error 400: Bad Request",
         message: "Dice History ID is not valid"
     })
 }

 //B3: Thao tác với cơ sở dữ liệu
 diceHistoryModel.findByIdAndDelete(diceHistoryId, (error) => {
     if(error) {
         return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
        return response.status(204).json({
            status: `Delete Dice History ID ${diceHistoryId} successfully!`
        })
     }
 })
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}