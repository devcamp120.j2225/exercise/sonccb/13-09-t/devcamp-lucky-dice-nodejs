//import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo course Schema
const prizeSchema = new Schema({
 // _id:{
 //  type:mongoose.Types.ObjectId,
 //  unique:true
 // },
 name:{
  type:String,
  required:true,
  unique:true
 },
 description:{
  type:String,
  required:false
 },
},{
 timestamps:true
});

module.exports = mongoose.model("Prize",prizeSchema);