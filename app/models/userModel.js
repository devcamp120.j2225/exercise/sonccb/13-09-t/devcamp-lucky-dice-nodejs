//import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo course Schema
const userSchema = new Schema({
 // _id:{
 //  type:mongoose.Types.ObjectId,
 //  unique:true
 // },
 username:{
  type:String,
  unique:true,
  required:true
 },
 firstname:{
  type:String,
  required:true
 },
 lastname:{
  type:String,
  required:true
 },
},{
 // _id:false,
 timestamps:true
});

module.exports = mongoose.model("User",userSchema);